package haw_landshut.de.pluto19gkw;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG ="xx SignIn Activity";

    // Declare UI Variables
    EditText mEditTextEmail;
    EditText mEditTextPassword;
    Button mButtonSignIn;
    Button mButtonForgotPassword;
    Button mButtonCreateAccount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // Initalise UI Variables
        mEditTextEmail = (EditText) findViewById( R.id.signInEMail);
        mEditTextPassword = (EditText) findViewById( R.id.signInPassword);

        mButtonSignIn = (Button) findViewById( R.id.signInButtonSignIn);
        mButtonCreateAccount = (Button) findViewById( R.id.signInButtonCreateAccount);
        mButtonForgotPassword = (Button ) findViewById(R.id.signInButtonForgotPassword);


        // Set Event Listener
        mButtonForgotPassword.setOnClickListener( this );
        mButtonCreateAccount.setOnClickListener( this );
        mButtonSignIn.setOnClickListener( this );
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "on Start called");
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId() ){
            case R.id.signInButtonCreateAccount:
                doCreateAccount();
                return;

            case R.id.signInButtonForgotPassword:
                doResetPassword();
                return;

            case R.id.signInButtonSignIn:
                doSignIn();
                return;

            default: return;

        }
    }

    void doResetPassword(){
        FirebaseAuth.getInstance().sendPasswordResetEmail(mEditTextEmail.getText().toString()).addOnCompleteListener(
                this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "Mail sent.", Toast.LENGTH_LONG).show();
                        } else {
                            // TODO: REmove later:
                            Toast.makeText(getApplication(), "Sending mail failed", Toast.LENGTH_LONG).show();
                            Log.d(TAG, task.getException().getLocalizedMessage());
                        }
                    }
                }
        );

    }

    void doCreateAccount(){
        Intent intent;
        intent = new Intent( getApplication(), CreateAccountActivity.class);
        startActivity( intent );
    }

    private void doSignIn() {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                mEditTextEmail.getText().toString(),
                mEditTextPassword.getText().toString()).addOnCompleteListener(
                this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "You are signed in", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(getApplication(), "Sign in failed", Toast.LENGTH_LONG).show();
                            Log.d(TAG, task.getException().getLocalizedMessage());
                        }
                    }
                }
        );
    }
}
