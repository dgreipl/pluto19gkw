package haw_landshut.de.pluto19gkw.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import haw_landshut.de.pluto19gkw.model.Post;

public class PostTestData {
    public static List<Post> postTestList = new ArrayList<Post>();

    public  static void createTestData(){
        postTestList.clear();
        long date = new Date().getTime();
        String body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sodales.";
        postTestList.add( new Post("1","Autor 1","Title 1", body, date+1));
        postTestList.add( new Post("2","Autor 2","Title 2", body, date+2));
        postTestList.add( new Post("3","Autor 3","Title 3", body, date+3));
        postTestList.add( new Post("4","Autor 4","Title 4", body, date+4));
    }

}
