package haw_landshut.de.pluto19gkw;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

import haw_landshut.de.pluto19gkw.model.Post;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "xx MainActivity";

    // Liste der emofangenen Posts
    private ArrayList<Post> mPostList;

    // Referenz auf die Listview in der layout datei
    private ListView mListView;

    // Adapter zwischen PostList und ListView
    private ArrayAdapter<Post> mAdapter;

    // User Firebase Authentication
    private FirebaseAuth mAuth;

    // Query auf die Database
    private Query mPostQuery;

    // The Eventlistener xxx_2
    ChildEventListener mChildEventListener;


    // TODO: Remove, nur zum Testen
    private final String EMAIL = "dietergreipl@gmail.com";
    private final String PASSWORD = "123456";

    private boolean mListenerIsRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "on Create");

        // Enable offline
        FirebaseDatabase.getInstance().setPersistenceEnabled( true );

        // Die Postliste mit der leeren Liste initialisieren.
        mPostList =  new ArrayList<Post>();

        // Initialisierung des Adaptes
        mAdapter = new ArrayAdapter<Post>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                mPostList
        ){
            @NonNull
            @Override
            public View getView(int position,
                                @Nullable View convertView,
                                @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView text1, text2;

                text1 = (TextView) view.findViewById(  android.R.id.text1);
                text2 = (TextView) view.findViewById(  android.R.id.text2);

                // Belege Textviews mit Werte aus dem Post
                Post post = getItem( getCount() - position - 1);
                text1.setText( post.author );
                text2.setText( post.body);
                return view;
            }
        };

        // Iinialisiere ListView
        mListView = findViewById( R.id.mainListViewMessages );

        // Verbinde Listview mit Adapter
        mListView.setAdapter( mAdapter );

        // Query initialisieren
        mPostQuery = FirebaseDatabase.getInstance().getReference().child("Posts/").limitToLast(3);

        // xxx_1
        mChildEventListener = getChildEventListener();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "on Start");

        if (FirebaseAuth.getInstance().getCurrentUser() == null){

            resetApp();
            Intent intent = new Intent( getApplication(), SignInActivity.class);
            startActivity( intent );

        } else {
            Log.d(TAG, "Aktueller Nutzer : " + FirebaseAuth.getInstance().getCurrentUser().getUid());

            // Veranlasse das Lesen der Posts
            // Cel an die Query anhängen.
            if (!mListenerIsRunning) {
                Log.d(TAG, "Starting Listener.");
                mPostList.clear();
                mAdapter.notifyDataSetChanged();

                // xxx
                mPostQuery.addChildEventListener( mChildEventListener );
                mListenerIsRunning = true;
            } else {
                Log.d(TAG, "Listener is already running.");
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_mainManageAccount:
                Intent intent = new Intent( getApplication(), ManageAccountActivity.class);
                startActivity( intent );
                return true;

            case R.id.menu_mainPost:
                intent = new Intent( getApplication(), PostActivity.class);
                startActivity( intent );
                return true;

            case R.id.menu_mainCrash:
                Crashlytics.getInstance().crash();
                return true;

            default:
                return true;
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "on Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "on Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "on Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "on Destroy");
    }


    private ChildEventListener getChildEventListener(){

        ChildEventListener cel = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Post p = Post.fromSnapshot( dataSnapshot );
                mPostList.add( p );

                // Adapter informieren, damit dieser Post angezeigt wird.
                mAdapter.notifyDataSetChanged();

                Toast.makeText(getApplicationContext(),
                        "Added : " + dataSnapshot.getKey(),
                        Toast.LENGTH_LONG).show();
                Log.d(TAG, "called onChild Added  :" + dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "called onChild Changed");

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "called onChild removed");

                String snapshotKey = dataSnapshot.getKey();

                for (int i=0; i < mPostList.size(); i++){
                    if (mPostList.get(i).firebaseKey.equals( snapshotKey)){

                        mPostList.remove ( i );
                        Log.d(TAG, "Deleted : " + snapshotKey);
                        break;
                    }
                }

                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "called onChild moved.");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "called onChild cancelled");

            }
        };

        return cel;

    }


    void resetApp(){
        Log.d(TAG, "Resetting the app, as no user is signed in");

        // Remove the listener, if is running
        if (mListenerIsRunning){
            Log.d(TAG, "Remove Child Eventlistner.");
            mPostQuery.removeEventListener( mChildEventListener );
            mListenerIsRunning = false;
        }

        mPostList.clear();
        mAdapter.notifyDataSetChanged();
    }
}
