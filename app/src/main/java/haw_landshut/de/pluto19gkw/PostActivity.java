package haw_landshut.de.pluto19gkw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;


public class PostActivity extends AppCompatActivity
        implements View.OnClickListener {

    EditText mPostTitle;
    EditText mPostBody;
    Button mPostButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        mPostTitle = (EditText) findViewById( R.id.postTitle);
        mPostBody  = (EditText) findViewById( R.id.postText);
        mPostButton = (Button) findViewById( R.id.postButtonPost);
        mPostButton.setOnClickListener( this );

        // Eingabefelder mit Testwerten vorbelegen
        mPostTitle.setText("Titel ");

        String body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ";
        mPostBody.setText( body );

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.postButtonPost:
                doPost();
                return;
            default:
                return;
        }
    }

    private void doPost() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Nur zur Sicherheit..., sollte im Regelbetrieb nicht auftreten.
        if (user == null) { return; }

        Map<String, Object> postMap = new HashMap<>();
        postMap.put("uid", user.getUid());
        postMap.put("author", user.getEmail());
        postMap.put("title", mPostTitle.getText().toString());
        postMap.put("body", mPostBody.getText().toString());
        postMap.put("timestamp", ServerValue.TIMESTAMP);

        DatabaseReference database = FirebaseDatabase
                .getInstance().getReference("Posts/");
        database.push().setValue( postMap );

        Toast.makeText(getApplicationContext(), "Post geschrieben", Toast.LENGTH_LONG).show();
    }
}
